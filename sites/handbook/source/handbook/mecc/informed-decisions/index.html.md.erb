---
layout: handbook-page-toc
title: "Informed decisions — Managing so Everyone Can Contribute (MECC)"
canonical_path: "/handbook/mecc/informed-decisions/"
description: Informed decisions — Managing so Everyone Can Contribute (MECC)
twitter_image: "/images/opengraph/all-remote.jpg"
twitter_image_alt: "GitLab remote team graphic"
twitter_site: "@gitlab"
twitter_creator: "@gitlab"
---

## On this page
{:.no_toc}

- TOC
{:toc}

{::options parse_block_html="true" /}

<%= partial("handbook/mecc/_mecc_overview.erb") %>

Informed decisions are a key attribute of [MECC](/handbook/mecc/). Unlike other management philosophies which prioritize human communication flows (e.g. cascading) in order to inform individuals, MECC creates a systems-level framework that empowers each individual to become informed *and* contribute to the informing data set(s). 

This scales more appropriately across time zones, functions, levels, human personalities, and consumption preferences. 

Below are the tenets of Informed Decisions within MECC. 

## Transparency

Conventional management philosophies may rely on intentional information silos or a "need-to-know-basis" model. This approach restricts transparency and optimizes for a reduction in misinformation. MECC flips this notion by requiring that information be [transparent by default](/handbook/values/#transparency) and optimizes for maximum contribution. This includes all information which is [explicitly not public](/handbook/communication/#not-public), and it requires transparency across all functions of a business. The key question answered is no longer, "*How does the organization get information to the right people at the right time?*", but rather, "*How does the organization create a system where everyone can consume information and  contribute, regardless of level or function?*"

In practice, an organization which implements MECC would use a project management system that allows finance, engineering, communications, design, marketing, and sales (as example departments) to view data related to all other functions. Instead of requiring that one function requests permission for access to another function's data, the system is designed wall-less by default. This means the onus of management is *not* to build an information sharing or request framework. Instead, the onus is to *educate* team members on how to add information in a systematized manner and how to search for data within the system.

Scalable leadership is effective leadership. By writing guidance down transparently — in a way that others can correct, validate, or otherwise contribute — leadership scales beyond an individual or team, and even beyond company tenure. A core outcome is individuals are less dependent on others for direction and guidance. This is not because those inputs are not valued, but because they are inputted to a transparent system as opposed to a silo or limited group. 

**Here's an example:** Onboarding new hires

- _In a conventional organization:_ Onboarding relies on knowledge sharing directly from current team members or managers to the new hire. This leaves room for ambiguity, lack of insight and connection to existing work, and reliance on individuals' time and availability, rather than empowering team members with access to the information they need at all times.
- _In an organization empowered by MECC:_ New hires are taught to adopt a [self-service and self-learning mindset](/company/culture/all-remote/self-service/) from day one. At GitLab, onboarding teaches team members [how to search the GitLab handbook](/handbook/tools-and-tips/searching/), [pay it forward](/handbook/values/#self-service-and-self-learning) with documentation, and [answer with a link](/company/culture/all-remote/self-service/#answer-with-a-link).


## Single Source of Truth (SSoT)

MECC deliberately structures data (policy, workflows, values, etc.) in a [single source of truth (SSoT)](/handbook/values/#single-source-of-truth). It is founded on the thesis that decisions are [better informed](/company/culture/all-remote/handbook-first-documentation/) when there is no such thing as a "latest version." There is only *the* version. 

Teams and functions may choose different mediums as the SSoT depending on the task and the nature of their work. MECC allows this type of SSoT flexibility, but requires that those who dictate the SSoT share that information transparently and [crosslink](/handbook/communication/#cross-link) where appropriate.

**Here's an example:** Meeting documentation

- _In a conventional organization:_ Each team uses their own disparate tools (static docs, physical whiteboards) to document meeting notes and outcomes. People outside the meeting or on other teams have no way of knowing where to access the information or how an outcome was reached.   
- _In an organization empowered by MECC:_ A [Google Doc](/handbook/communication/#google-docs) may be the SSoT for a [1-to-1](/handbook/leadership/1-1/) agenda between a people manager and a direct report, while a MURAL board may be the SSoT for a design team's ideation [meeting](/company/culture/all-remote/meetings/). The design team is responsible for sharing the link to its MURAL board in a project management system which is accessible to (and searchable by) the entire company. 


## Low-context communication

[Low-context communication](/company/culture/all-remote/effective-communication/) is marked by explicit *over* implicit, direct *over* indirect, simple *over* complex, and comprehensive *over* narrow. It is called low-context communications because individuals are not expected to have knowledge of others' history and background. In a work setting, this is common; most team members do not have preexisting relationships with their colleagues prior to joining the same team. By establishing a high bar for including as much context as is known in all information exchanges, decisions are enabled to be more informed. 

Each business function may have unique expectations on low-context communication (e.g. what classifies as low-context in sales may not in engineering). MECC requires that each function exhibit qualities of low-context communication which are reasonable for the demands of that function. If decisions within a function appear to be ill-informed, audit the expectations on context first. 

**Here's an example:** Companywide announcements

- _In a conventional organization:_ A department leader uses the "updates" portion of a companywide call to briefly mention a new process that will impact all teams. Not everyone is in attendance. Even those on the call have no additional context about what changed, or a documented way to share and apply this information across their own teams. 
- _In an organization empowered by MECC:_ A department leader sends out a [companywide message](/handbook/communication/#how-to-make-a-company-wide-announcement) to a Slack channel that includes every team member. Crucially, this message does not include *only* the news, but a link to a GitLab merge request *detailing what changed* ([diffs](https://docs.gitlab.com/ee/development/diffs.html)). The [merge request](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/104440) which added the very copy you're reading now sheds light into the practice of low-context communication. The [DRI](/handbook/people-group/directly-responsible-individuals/) for the change also shares a link to the handbook and/or project page ("the news"), the merge request includes context on what's changing, and details on where to ask questions and contribute new iterations (including an optional [Ask Me Anything (AMA)](/handbook/communication/ask-me-anything/) session). Managers and team members have enough context to share feedback and apply these changes to their own teams in an informed way.

## Situational leadership

Adopting a [situational leadership strategy](/blog/2021/11/19/situational-leadership-strategy/) brings an added layer of intelligence to the way leaders manage each indiviudal, project, and decision. It acknowledges that leaders should adapt the way they communicate, provide guidance, and delegate work based on a list of weighed factors and considerations. This strategy enables more informed decisions, prevents a reliance on status quo, and presents new growth opportunities for team members.

When it comes to MECC, [situational leadership](https://situational.com/situational-leadership/) does not only apply to people managers. It also applies to individual contributors, as everyone is expected to be a [manager of one](/handbook/leadership/#managers-of-one). 

**Here's an example:** Launching a new project

- _In a conventional organization:_ A manager assigns workstreams for a new project to various team members based on tradition and job title. They do not inherently take into account factors like task urgency, the manger's own skill, time available, time needed, the manager's emotion, feedback effort/allocation, a report's state of mind, and relationship duration. 
- _In an organization empowered by MECC:_ When kicking off a new project, a manager is more open-minded. A [curated list of factors](/blog/2021/11/19/situational-leadership-strategy/) inform a manager's approach to assigning tasks, but those are only the start. By managing so everyone can contribute, the manager may also seek intra-company volunteers (often from unrelated departments/functions) and for advice on social media (outside of the company). This leads to more informed decisions through a more diverse array of inputs.

## Inclusivity 

Decisions are better informed overall when they include a maximally [diverse array of perspectives](/handbook/values/#seek-diverse-perspectives). In MECC, a [bias for asynchronous communication](/handbook/values/#bias-towards-asynchronous-communication) fosters [inclusion](/company/culture/inclusion/). By defaulting to written, asynchronous sharing, everyone contributes in the same size font. People of all backgrounds, abilities, and work styles are invited to participate in a way that serves their needs. The best idea wins, not the loudest voice in the meeting. 

MECC frees contributors from the conventional bounds of time zones and meetings. This generates more informed contributions from more parties, more thoughtful conversation, and more archived context for retrospectives and evaluations. 

**Here's an example:** Change management

- _In a conventional organization:_ A team has a closed-door meeting to make a decision that impacts the whole company. They announce the change on a companywide call hosted in one time zone.      
- _In an organization empowered by MECC:_ A team recognizes the need for iterations to a process that impacts the whole company. They document this need and share a project page where the entire team can learn more and contribute feedback asynchronously. Ultimately, [the DRI](/handbook/people-group/directly-responsible-individuals/) comes to a decision that is more informed because it includes a diverse range of perspectives. The feedback is also well-documented for future reference and iterations. 

---

Informed decisions enable [Fast decisions](/handbook/mecc/fast-decisions/), the second tenet of [MECC](/handbook/mecc/).
