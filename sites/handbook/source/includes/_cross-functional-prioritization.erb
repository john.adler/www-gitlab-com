### Cross-Functional Prioritization

To support GitLab's long-term product health and stability while keeping the pace of new features for users, teams are asked to plan their milestones with an appropriate ratio of `type::feature`, `type::maintenance`, and `type:bug` work. A team's ratio might change over time and different teams may have different ratios. Factors that influence what ratio is appropriate for a given team include the [product category maturity](https://about.gitlab.com/direction/maturity/), the area of the product they are working in, and the evolving needs of GitLab the business and GitLab the product. Teams should review labeling for accuracy as well as minimizing the number of `type::undefined` items. This allows us to review the plans at the group, section, and company level with team members with those perspectives to insure we are appropriately prioritizing based on cross-functional needs and perspectives.

For more details on these three work types, please see the section on [work type classification](/handbook/engineering/metrics/#work-type-classification).  The development EM is the DRI to ensure that the merge requests are accurateliy labeled.

#### Prioritization for feature, maintenance, and bugs

Our backlog should be prioritized on an ongoing basis. Prioritization will be done via quad planning (collaboration between product, development, quality, UX) with PM as the DRI for the milestone plan. PMs, EMs, Quality, and UX will provide the following:

1. Product Manager provides prioritized `type::feature` issues
1. Engineering Manager in development provides prioritized `type::maintenance` issues
1. [Quality](https://about.gitlab.com/handbook/engineering/quality/quality-engineering/#milestone-planning) provides prioritized `type::bug` issues

_Note: UX-related work items would be prioritized in accordance with the appropriate sub-types. UX related bugs are included in the automated process (S1/2 and so on), UX-related maintenance items will be included in the EM's prioritized list, Product (feature) UX items will have been included as part of our normal [Product Development Flow](https://about.gitlab.com/handbook/product-development-flow/)._ 

The DRIs of these three core areas will work collaboratively to ensure the overall prioritization of the backlog is in alignment with [section direction](https://about.gitlab.com/direction/#devops-stages) or any other necessary product and business needs. If a team is not assigned a Product Designer then there is no UX counterpart needed for prioritization purposes. PMs will prioritize the final plan for a given milestone.

It is recommended that teams use a Cross-functional Prioritization Board like [this example](https://gitlab.com/groups/gitlab-org/-/boards/4309441?not[label_name][]=UX&label_name[]=group%3A%3Athreat%20insights) which provides columns for `type::feature`, `type::maintenance`, and `type::bug` issues. Issues may be reordered by drag and drop.

**Note:** Each team is encouraged to create their own board as the example board above belongs to the Threat Insights team. Please do not modify this board unless you are a member of the Threat Insights team.

Drag and drop reordering is also supported in the issues list by sorting by `Manual` ([example](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=relative_position&state=opened&label_name%5B%5D=group%3A%3Athreat%20insights&label_name%5B%5D=type%3A%3Amaintenance&first_page_size=100)). You may find this view to be more effective when focusing on a specific type, or when working against large backlog. When you adjust the order of issues in the _Manual_ list view, it's automatically reflected in the board view, so the order is consistent between both views.

Notes: 

1. It may be useful to filter out certain issue types like `UX` that aren't relevant to implementation issues.
1. Toggle off "show labels" in the board for a cleaner view.

#### Planning for the milestone

The Product Manager is responsible for planning each milestone. Product Managers are also responsible for ensuring that their team's target ratios are maintained over time.

1. Using the prioritized backlogs and guidance from their EM, QEM, and UX counterparts, the Product Manager fills the next milestone with a set of issues using the agreed work type ratios for their team. 
   1. Pick based on what you think a common backlog would look like and note the expected ratio of features/maintenance/bugs.
   1. Teams working on net-new product functionality may have a much higher feature work percentage. 
   1. Conversly, teams with more mature product categories may have more maintence work.
1. Sequencing of work within the milestone should reflect that we [plan ambitiously](/handbook/product/product-principles/#how-this-impacts-planning). This means not every issue will be delivered. Be aware that this can skew your ratios over time if subsequent milestones are not properly re-balanced.

It is recommended to use the your team's same [Cross-functional Prioritization board](/handbook/cross-functional-prioritization#prioritization-for-feature-maintenance-and-bugs) for milestone planning.

Add the `milestone` ([example](https://gitlab.com/groups/gitlab-org/-/boards/4309441?label_name[]=group%3A%3Athreat%20insights&milestone_title=15.1)) to review the milestone plan. The board will show the number of issues and cumulative issue weights for `type::feature`, `type::maintenance`, and `type::bug` issues.

#### Cross-Functional Dashboard Reviews

The primary goals of this review exercise is for teams to:
1. Keep `undefined` MRs under 5% of all MRs merged for a given calendar month, where `undefined` MRs refers to any MRs without a `type::` label
1. Evaluate the percentage ratios of completed work (feature / maintenance / bug) for the previous milestone against the team's planned ratio for that milestone
1. Iterate on the next milestone plan according to results of the evaluation
1. Agree among the quad on the next milestone's ratio
1. Share learnings from the review with peers and your section

These reviews will use cross-functional dashboards embedded on each team's handbook page that serve as the SSOT when reviewing `type::` labels of merged MRs.

Note that the review collaboration can be done in a way that's most effective for the team, either synchronously (e.g. scheduled recurring call) or asynchronously (e.g. issues), as long as the previous reviews are well documented (with historical tracking).

Three types of reviews happen, where the cadence and attendees vary at each level:
1. Monthly Stage Group Review with the team's quad leadership (EM, PM, UX, Quality)
1. Quarterly Sub-department Review with the Section leadership (Senior EMs / Director of Engineering, Group PM / Director of Product, UX Manager, Quality EM)
1. Quarterly Department Review with the CTO, VP of PM, VP of UX, VP of Quality, VP of Development 

Questions to answer:
1. Are the undefined MR types over or under 5%?  If over 5%, what improvements can be made? 
 - Stage groups: Ensure issues and MRs are adequately tracked with a `type` label, or leveraging the `/copy_metdata` command on merged MRs.
 - Sub-departments and Department levels: Which teams are impacting the subdepartment or department average? What action items are being taken to address these?
1. Did we meet our agreed ratio goals?
 - At all levels: Review the goals of type percentages and discuss their accuracy and trends. What went well and what improvements should be made?
 - Stage groups: If adjustments need to be made, update your team handbook page.
 
